/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

import java.util.List;
import model.Employee;

/**
 *
 * @author S4671314
 */
public interface EmployeeDAOInterface {
    public List<Employee> getAllEmployees();
    public int addEmployee(Employee employee);
    public void updateEmployee(Employee emp);
    public void deleteEmployee(Employee emp);
}
