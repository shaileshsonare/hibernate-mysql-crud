/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.Iterator;
import java.util.List;
import model.Employee;
import model.interfaces.EmployeeDAOInterface;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author S4671314
 */
public class EmployeeDAO implements EmployeeDAOInterface {

    @Override
    public List<Employee> getAllEmployees() {
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        Session session = sf.openSession();
        
        String hql = "FROM Employee where id = 1";
        
        List<Employee> list = session.createQuery(hql).list();
        
        for(Iterator iterator = list.iterator(); iterator.hasNext();) {
            Employee e = (Employee)iterator.next();
            
            System.out.println(e.getName());
        }
        
        return list;
    }

    @Override
    public int addEmployee(Employee employee) {
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        Session session = sf.openSession();
        Transaction t = session.beginTransaction();
        int id = (Integer)session.save(employee);
        t.commit();
        System.out.println("Interted Id " + id);
        
        return id;
    }

    @Override
    public void updateEmployee(Employee emp) {
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        Session session = sf.openSession();
        Transaction t = session.beginTransaction();
        session.update(emp);
        t.commit();
    }

    @Override
    public void deleteEmployee(Employee emp) {
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        Session session = sf.openSession();
        Transaction t = session.beginTransaction();
        session.delete(emp);
        t.commit();
    }
    
}
